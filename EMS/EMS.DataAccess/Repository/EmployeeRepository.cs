﻿using EMS.DataAccess.Interfaces;
using EMS.Model.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace EMS.DataAccess.Repository
{
    public class EmployeeRepository : IEmployeeRepository
    {
        #region [Private Variables]
        private string _sqlConnection;
        #endregion

        #region [Constructor]
        public EmployeeRepository(string connectionString)
        {
            _sqlConnection = connectionString;
        }
        #endregion

        #region [Repository Methods]
        public Result CreateNewEmployee(Employee employee)
        {
            using (var connection = Connect())
            {
                using (var command = new SqlCommand(Constants.Procedures.SP_ADD_EDIT_DELETE_EMPLOYEE, connection))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    connection.Open();
                    command.Parameters.Add(new SqlParameter("@Mode", (int)Constants.Mode.Add));
                    command.Parameters.Add(new SqlParameter("@FirstName", employee.FirstName));
                    command.Parameters.Add(new SqlParameter("@MiddleName", employee.MiddleName));
                    command.Parameters.Add(new SqlParameter("@LastName", employee.LastName));
                    command.Parameters.Add(new SqlParameter("@UserName", employee.UserName));
                    command.Parameters.Add(new SqlParameter("@Password", employee.Password));
                    command.Parameters.Add(new SqlParameter("@DateOfBirth", employee.DateOfBirth));
                    command.Parameters.Add(new SqlParameter("@MobileNumber", employee.MobileNumber));
                    command.Parameters.Add(new SqlParameter("@EmailAddress", employee.Email));
                    command.Parameters.Add(new SqlParameter("@Address", employee.Address));

                    var resOutParam = new SqlParameter("@res", System.Data.SqlDbType.Int, 8);
                    var errIdOutParam = new SqlParameter("@errorId", System.Data.SqlDbType.Int, 8);
                    var errMsgOutParam = new SqlParameter("@error_mesasge", System.Data.SqlDbType.VarChar, 100);

                    resOutParam.Direction = System.Data.ParameterDirection.Output;
                    errIdOutParam.Direction = System.Data.ParameterDirection.Output;
                    errMsgOutParam.Direction = System.Data.ParameterDirection.Output;

                    command.Parameters.Add(resOutParam);
                    command.Parameters.Add(errIdOutParam);
                    command.Parameters.Add(errMsgOutParam);
                    command.ExecuteNonQuery();

                    var newId = Convert.ToInt32(command.Parameters["@res"].Value);
                    var errId = Convert.ToInt32(command.Parameters["@errorId"].Value);
                    var errMsg = Convert.ToString(command.Parameters["@error_mesasge"].Value);

                    return new Result
                    {
                        ErrorId = errId,
                        ErrorMessage = errMsg,
                        Id = newId
                    };
                }
            }
        }

        public Result DeleteEmployee(int empId)
        {
            using (var connection = Connect())
            {
                using (var command = new SqlCommand(Constants.Procedures.SP_ADD_EDIT_DELETE_EMPLOYEE, connection))
                {
                    connection.Open();
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Mode", (int)Constants.Mode.Delete));
                    command.Parameters.Add(new SqlParameter("@Id", empId));
                    var resOutParam = new SqlParameter("@res", System.Data.SqlDbType.Int);
                    var errIdOutParam = new SqlParameter("@errorId", System.Data.SqlDbType.Int);
                    var errMsgOutParam = new SqlParameter("@error_mesasge", System.Data.SqlDbType.VarChar, 100);

                    resOutParam.Direction = System.Data.ParameterDirection.Output;
                    errIdOutParam.Direction = System.Data.ParameterDirection.Output;
                    errMsgOutParam.Direction = System.Data.ParameterDirection.Output;

                    command.Parameters.Add(resOutParam);
                    command.Parameters.Add(errIdOutParam);
                    command.Parameters.Add(errMsgOutParam);
                    command.ExecuteNonQuery();

                    var id = Convert.ToInt32(command.Parameters["@res"].Value);
                    var errId = Convert.ToInt32(command.Parameters["@errorId"].Value);
                    var errMsg = Convert.ToString(command.Parameters["@error_mesasge"].Value);

                    return new Result
                    {
                        ErrorId = errId,
                        ErrorMessage = errMsg,
                        Id = id
                    };
                }
            }
        }

        public List<Employee> GetAllEmployees()
        {
            var lstEmployee = new List<Employee>();
            using (var connection = Connect())
            {
                using (var command = new SqlCommand(Constants.Procedures.SP_GET_EMPLOYEE, connection))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    connection.Open();
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        lstEmployee.Add(new Employee
                        {
                            Id = reader["ID"] != DBNull.Value ? Convert.ToInt32(reader["ID"]) : 0,
                            FirstName = reader["FirstName"] != DBNull.Value ? Convert.ToString(reader["FirstName"]) : string.Empty,
                            MiddleName = reader["MiddleName"] != DBNull.Value ? Convert.ToString(reader["MiddleName"]) : string.Empty,
                            LastName = reader["LastName"] != DBNull.Value ? Convert.ToString(reader["LastName"]) : string.Empty,
                            UserName = reader["UserName"] != DBNull.Value ? Convert.ToString(reader["UserName"]) : string.Empty,
                            Password = reader["Password"] != DBNull.Value ? Convert.ToString(reader["Password"]) : string.Empty,
                            DateOfBirth = reader["DateOfBirth"] != DBNull.Value ? Convert.ToDateTime(reader["DateOfBirth"]) : DateTime.MinValue,
                            MobileNumber = reader["MobileNumber"] != DBNull.Value ? Convert.ToString(reader["MobileNumber"]) : string.Empty,
                            Email = reader["EmailAddress"] != DBNull.Value ? Convert.ToString(reader["EmailAddress"]) : string.Empty,
                            Address = reader["Address"] != DBNull.Value ? Convert.ToString(reader["Address"]) : string.Empty,
                        });
                    }
                }
            }
            return lstEmployee;
        }

        public Employee GetEmployeeById(int id)
        {
            var employee = new Employee();
            using (var connection = Connect())
            {
                using (var command = new SqlCommand(Constants.Procedures.SP_GET_EMPLOYEE, connection))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Id", id));
                    connection.Open();
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        employee = new Employee
                        {
                            Id = reader["ID"] != DBNull.Value ? Convert.ToInt32(reader["ID"]) : 0,
                            FirstName = reader["FirstName"] != DBNull.Value ? Convert.ToString(reader["FirstName"]) : string.Empty,
                            MiddleName = reader["MiddleName"] != DBNull.Value ? Convert.ToString(reader["MiddleName"]) : string.Empty,
                            LastName = reader["LastName"] != DBNull.Value ? Convert.ToString(reader["LastName"]) : string.Empty,
                            UserName = reader["UserName"] != DBNull.Value ? Convert.ToString(reader["UserName"]) : string.Empty,
                            Password = reader["Password"] != DBNull.Value ? Convert.ToString(reader["Password"]) : string.Empty,
                            DateOfBirth = reader["DateOfBirth"] != DBNull.Value ? Convert.ToDateTime(reader["DateOfBirth"]) : DateTime.MinValue,
                            MobileNumber = reader["MobileNumber"] != DBNull.Value ? Convert.ToString(reader["MobileNumber"]) : string.Empty,
                            Email = reader["EmailAddress"] != DBNull.Value ? Convert.ToString(reader["EmailAddress"]) : string.Empty,
                            Address = reader["Address"] != DBNull.Value ? Convert.ToString(reader["Address"]) : string.Empty,
                        };
                    }
                }
            }
            return employee;
        }

        public Employee GetEmployeeByUserName(string userName)
        {
            var employee = new Employee();
            using (var connection = Connect())
            {
                using (var command = new SqlCommand(Constants.Procedures.SP_GET_EMPLOYEE, connection))
                {
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@UserName", userName));
                    connection.Open();
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        employee = new Employee
                        {
                            Id = reader["ID"] != DBNull.Value ? Convert.ToInt32(reader["ID"]) : 0,
                            FirstName = reader["FirstName"] != DBNull.Value ? Convert.ToString(reader["FirstName"]) : string.Empty,
                            MiddleName = reader["MiddleName"] != DBNull.Value ? Convert.ToString(reader["MiddleName"]) : string.Empty,
                            LastName = reader["LastName"] != DBNull.Value ? Convert.ToString(reader["LastName"]) : string.Empty,
                            UserName = reader["UserName"] != DBNull.Value ? Convert.ToString(reader["UserName"]) : string.Empty,
                            Password = reader["Password"] != DBNull.Value ? Convert.ToString(reader["Password"]) : string.Empty,
                            DateOfBirth = reader["DateOfBirth"] != DBNull.Value ? Convert.ToDateTime(reader["DateOfBirth"]) : DateTime.MinValue,
                            MobileNumber = reader["MobileNumber"] != DBNull.Value ? Convert.ToString(reader["MobileNumber"]) : string.Empty,
                            Email = reader["EmailAddress"] != DBNull.Value ? Convert.ToString(reader["EmailAddress"]) : string.Empty,
                            Address = reader["Address"] != DBNull.Value ? Convert.ToString(reader["Address"]) : string.Empty,
                        };
                    }
                }
            }
            return employee;
        }

        public Result UpdateEmployee(Employee employee)
        {
            using (var connection = Connect())
            {
                using (var command = new SqlCommand(Constants.Procedures.SP_ADD_EDIT_DELETE_EMPLOYEE, connection))
                {
                    connection.Open();
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.Add(new SqlParameter("@Mode", (int)Constants.Mode.Modify));
                    command.Parameters.Add(new SqlParameter("@Id", employee.Id));
                    command.Parameters.Add(new SqlParameter("@FirstName", employee.FirstName));
                    command.Parameters.Add(new SqlParameter("@MiddleName", employee.MiddleName));
                    command.Parameters.Add(new SqlParameter("@LastName", employee.LastName));
                    command.Parameters.Add(new SqlParameter("@UserName", employee.UserName));
                    command.Parameters.Add(new SqlParameter("@Password", employee.Password));
                    command.Parameters.Add(new SqlParameter("@DateOfBirth", employee.DateOfBirth));
                    command.Parameters.Add(new SqlParameter("@MobileNumber", employee.MobileNumber));
                    command.Parameters.Add(new SqlParameter("@EmailAddress", employee.Email));
                    command.Parameters.Add(new SqlParameter("@Address", employee.Address));

                    var resOutParam = new SqlParameter("@res", System.Data.SqlDbType.Int);
                    var errIdOutParam = new SqlParameter("@errorId", System.Data.SqlDbType.Int);
                    var errMsgOutParam = new SqlParameter("@error_mesasge", System.Data.SqlDbType.VarChar, 100);

                    resOutParam.Direction = System.Data.ParameterDirection.Output;
                    errIdOutParam.Direction = System.Data.ParameterDirection.Output;
                    errMsgOutParam.Direction = System.Data.ParameterDirection.Output;

                    command.Parameters.Add(resOutParam);
                    command.Parameters.Add(errIdOutParam);
                    command.Parameters.Add(errMsgOutParam);
                    command.ExecuteNonQuery();

                    var newId = Convert.ToInt32(command.Parameters["@res"].Value);
                    var errId = Convert.ToInt32(command.Parameters["@errorId"].Value);
                    var errMsg = Convert.ToString(command.Parameters["@error_mesasge"].Value);

                    return new Result
                    {
                        ErrorId = errId,
                        ErrorMessage = errMsg,
                        Id = newId
                    };
                }
            }
        }
        #endregion

        #region [Private Methods]

        private SqlConnection Connect()
        {
            return new SqlConnection(_sqlConnection);
        }
        #endregion
    }
}
