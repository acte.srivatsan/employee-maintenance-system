﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EMS.Model.Models
{
    public class Constants
    {
        public class Procedures
        {
            public const string SP_GET_EMPLOYEE = "SP_GET_EMPLOYEE";
            public const string SP_ADD_EDIT_DELETE_EMPLOYEE = "SP_ADD_EDIT_DELETE_EMPLOYEE";
        }

        public enum Mode
        {
            Add=1,
            Modify=2,
            Delete=3
        }
    }

    public class Result
    {
        public int Id { get; set; }
        public int ErrorId { get; set; }
        public string ErrorMessage { get; set; }
    }
}
